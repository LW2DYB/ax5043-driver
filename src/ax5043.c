/*
 *  AX5043 OS-independent driver
 *
 *  Copyright (C) 2019 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ax5043.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>


#define HDLC_SYNC_FLAG 0x7E
#define PI 3.14159265358979323846

/**
 * The index inside the \ref TX buffer buffer that is currently been sent
 */
static size_t g_tx_buf_idx = 0;
/**
 * How many bytes of the \ref g_tx_buf are remaining for the frame transmission
 */
static uint32_t g_tx_remaining = 0;

/**
 * Indicates if a TX is currently active
 */
static volatile uint8_t g_tx_active = 0;

/**
 * TX gain lookup table. From 15dBm to -10dBm with 1dBm steps.
 * Contain the combined value of the two register : TXPWRCOEFFB1 and TXPWRCOEFFB0.
 */
static const uint16_t g_tx_gain_lookup_table[26] = { 0x0FFF, 0x0F00, 0x0C00,
                                                     0x09D4, 0x0800, 0x0700, 0x0600, 0x057C, 0x04A9, 0x0406, 0x03D6, 0x035E,
                                                     0x02EB, 0x0290, 0x0244, 0x0207, 0x01E0, 0x01AF, 0x017F, 0x0156, 0x0132,
                                                     0x010F, 0x00EC, 0x00D1, 0x00BF, 0x00AA,
                                                   };

static struct ax5043_conf *ax5043_conf = NULL;

static inline int
set_tx_black_magic_regs();

static int
spi_read(struct ax5043_conf *conf, uint8_t *out, uint16_t reg,
         uint32_t len);

static int
spi_write(struct ax5043_conf *conf, uint16_t reg, const uint8_t *in,
          uint32_t len);

static int
spi_read_8(struct ax5043_conf *conf, uint8_t *out,
           uint16_t reg);

static int
spi_read_16(struct ax5043_conf *conf, uint16_t *out, uint16_t reg);

int
spi_read_24(struct ax5043_conf *conf, uint32_t *out, uint16_t reg);

static int
spi_read_32(struct ax5043_conf *conf, uint32_t *out, uint16_t reg);


static int
spi_write_8(struct ax5043_conf *conf, uint16_t reg,
            uint8_t in);

static int
spi_write_16(struct ax5043_conf *conf, uint16_t reg,
             uint16_t in);

static int
spi_write_24(struct ax5043_conf *conf, uint16_t reg,
             uint32_t in);

static int
spi_write_32(struct ax5043_conf *conf, uint16_t reg,
             uint32_t in);

static int
set_pll_params(struct ax5043_conf *conf);

static int
autoranging(struct ax5043_conf *conf, freq_mode_t mode);

static int
set_tx_synth(struct ax5043_conf *conf, freq_mode_t mode);

static int
set_rx_synth(struct ax5043_conf *conf, freq_mode_t mode);

static int
wait_xtal(struct ax5043_conf *conf, uint32_t timeout_ms);

static void
init_dump_regs(struct reg_dump *dump);

/**
 * Checks if the AX5043 handler is valid
 * @param conf the AX5043 configuration handler pointer
 * @return true if it is valid, false otherwise
 */
static bool
ax5043_conf_valid(struct ax5043_conf *conf)
{
	if (!conf) {
		return false;
	}
	return true;
}


static inline int
tx_fifo_irq_enable(struct ax5043_conf *conf, uint8_t enable)
{
	int ret;
	uint16_t val =
	        enable ?
	        AX5043_IRQMFIFOTHRFREE | AX5043_IRQMRADIOCTRL : AX5043_IRQMRADIOCTRL;
	/* Enable FIFO IRQs */
	ax5043_irq_enable(0);
	ret = spi_write_16(conf, AX5043_REG_IRQMASK1, val);
	if (ret) {
		return ret;
	}
	ax5043_irq_enable(1);
	return AX5043_OK;
}

uint32_t
reverse_bits(uint32_t x)
{
	uint32_t y = 0x55555555;
	x = (((x >> 1) & y) | ((x & y) << 1));
	y = 0x33333333;
	x = (((x >> 2) & y) | ((x & y) << 2));
	y = 0x0f0f0f0f;
	x = (((x >> 4) & y) | ((x & y) << 4));
	y = 0x00ff00ff;
	x = (((x >> 8) & y) | ((x & y) << 8));
	return ((x >> 16) | (x << 16));
}

/**
 * Checks if the AX5043 IC is ready for use. This includes proper values
 * on the \ref conf structure and succesfull completion of the \ref init()
 * function
 * @param conf the AX5043 configuration handler
 * @return true if the IC can be used, false otherwise
 */
bool
ax5043_ready(struct ax5043_conf *conf)
{
	return ax5043_conf_valid(conf) && conf->priv.ready;
}

/**
 * Resets the AX5043 IC and set it to \a POWEDOWN mode.
 * Any RF sythnesizer setup is invalidated after the call of this
 * function
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_reset(struct ax5043_conf *conf)
{
	int ret;
	uint8_t val;

	if (!ax5043_conf_valid(conf)) {
		return -AX5043_INVALID_PARAM;
	}

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	struct ax5043_conf_priv *conf_priv = &conf->priv;
	conf_priv->rf_init = 0;
	ax5043_spi_cs(1);
	ax5043_usleep(100);
	ax5043_spi_cs(0);
	ax5043_usleep(100);
	ax5043_spi_cs(1);
	ax5043_usleep(100);

	/* Reset the chip using the appropriate register */
	val = BIT(7);
	ret = spi_write_8(conf, AX5043_REG_PWRMODE, val);
	if (ret) {
		return ret;
	}
	ax5043_usleep(100);
	/* Clear the reset bit, but keep REFEN and XOEN */
	ret = spi_read_8(conf, &val, AX5043_REG_PWRMODE);
	if (ret) {
		return ret;
	}
	val &= (BIT(6) | BIT(5));
	ret = spi_write_8(conf, AX5043_REG_PWRMODE, val);
	if (ret) {
		return ret;
	}
	ax5043_usleep(100);

	ret = ax5043_set_power_mode(conf, POWERDOWN);
	if (ret) {
		return ret;
	}
	return AX5043_OK;
}

/**
 * Sets the crystal capabilities and crystal related internal parameters
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
static int
set_xtalcap(struct ax5043_conf *conf)
{
	int ret = AX5043_OK;
	if (conf->xtal.freq < MIN_FXTAL || conf->xtal.freq > MAX_FXTAL) {
		return -AX5043_INVALID_PARAM;
	}

	if (conf->xtal.freq > 24.8e6) {
		conf->priv.f_xtaldiv = 2;
	} else {
		conf->priv.f_xtaldiv = 1;
	}

	uint8_t f10 = 0x0;
	switch (conf->xtal.type) {
		case TCXO:
			ret = spi_write_8(conf, AX5043_REG_XTALCAP, 0);
			if (conf->xtal.freq > 43e6) {
				f10 = 0xD;
			} else {
				f10 = 0x4;
			}
			break;
		case XTAL:
			if (conf->xtal.freq > 43e6) {
				f10 = 0xD;
			} else {
				f10 = 0x3;
			}
		case XTAL_INTERNAL:
			if (conf->xtal.capacitance < 8.0f) {
				ret = spi_write_8(conf, AX5043_REG_XTALCAP, 0);
			} else {
				uint32_t c = 2 * (conf->xtal.capacitance - 8.0f);
				ret = spi_write_8(conf, AX5043_REG_XTALCAP, c & 0xFF);
			}
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	if (ret) {
		return ret;
	}

	if (f10 != 0) {
		ret = spi_write_8(conf, 0xF10, f10);
		if (ret) {
			return ret;
		}
	}

	if (conf->xtal.type == TCXO) {
		ret = spi_write_8(conf, 0xF11, 0x0);
	} else {
		ret = spi_write_8(conf, 0xF11, 0x7);
	}

	/* Write the performance register F35 based on the XTAL frequency */
	if (conf->priv.f_xtaldiv == 1) {
		ret = spi_write_8(conf, 0xF35, 0x10);
	} else {
		ret = spi_write_8(conf, 0xF35, 0x11);
	}
	if (ret) {
		return ret;
	}
	return ret;
}

/**
 * Initialization routine for the AX5043 IC
 * @param conf the AX5043 configuration handler with initialized the
 * necessary field required for the initial setup of the driver
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_init(struct ax5043_conf *conf)
{
	int ret;
	uint8_t revision;
	uint8_t val;

	if (!ax5043_conf_valid(conf)) {
		return -AX5043_INVALID_PARAM;
	}

	if (!conf->rx_buffer || !conf->rx_buffer_len || !conf->tx_buffer
	    || !conf->tx_buffer_len) {
		return -AX5043_INVALID_PARAM;
	}

	memset(&conf->priv, 0, sizeof(struct ax5043_conf_priv));

	switch (conf->vco) {
		case VCO_INTERNAL:
		case VCO_EXTERNAL:
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	struct ax5043_conf_priv *conf_priv = &conf->priv;
	conf_priv->rf_init = 0;
	/* Try first to read the revision register of the AX5043 */
	ret = spi_read_8(conf, &revision, AX5043_REG_REV);
	if (ret) {
		return ret;
	}

	if (revision != AX5043_REV) {
		return -AX5043_NOT_FOUND;
	}

	/* To ensure communication try to write and read the scratch register */
	val = AX5043_SCRATCH_TEST;
	ret = spi_write_8(conf, AX5043_REG_SCRATCH, val);
	if (ret) {
		return ret;
	}

	val = 0x0;
	ret = spi_read_8(conf, &val, AX5043_REG_SCRATCH);
	if (ret) {
		return ret;
	}

	if (val != AX5043_SCRATCH_TEST) {
		return -AX5043_NOT_FOUND;
	}

	/* From now on the IC is considered ready */
	conf_priv->ready = 1;
	conf_priv->freqa = 0;
	conf_priv->freqb = 0;
	conf_priv->freqa_req = 0;
	conf_priv->freqb_req = 0;
	conf_priv->auto_rng_freqa = 0;
	conf_priv->auto_rng_freqb = 0;
	ret = ax5043_reset(conf);
	if (ret) {
		return ret;
	}

	ret = set_xtalcap(conf);
	if (ret) {
		return ret;
	}

	ret = set_pll_params(conf);
	if (ret) {
		return ret;
	}

	/* FIFO maximum chunk */
	ret = spi_write_8(conf, AX5043_REG_PKTCHUNKSIZE,
	                  AX5043_PKTCHUNKSIZE_240);
	if (ret) {
		return ret;
	}

	/* Set an internal copy for the IRQ handler */
	ax5043_conf = conf;

	/*
	 * Set the FIFO IRQ threshold. During TX, when the free space is larger
	 * than this threshold, an IRQ is raised
	 */
	ret = spi_write_16(conf, AX5043_REG_FIFOTHRESH1, AX5043_FIFO_FREE_THR);
	if (ret) {
		return ret;
	}

	ret = set_tx_black_magic_regs(conf);
	if (ret) {
		return ret;
	}

	/*
	 * Set some flags to get RX information at every decoded frame
	 * This extra information is:
	 *  - Frequency offset
	 *  - RF frequency offset
	 *  - Data rate offset
	 *  - RSSI
	 */
	ret = spi_write_8(conf, AX5043_REG_PKTSTOREFLAGS, 0x1E);
	if (ret) {
		return ret;
	}

#if AX5043_DEBUG
	init_dump_regs(&conf->dump);
#endif
	return AX5043_OK;
}

/**
 * SPI chip select pin level
 * @param cs set 1 to set the CS signal high, 0 to set it low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
ax5043_spi_cs(uint8_t cs)
{
	return AX5043_OK;
}

/**
 * Writes and reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
ax5043_spi_trx(uint8_t *rx, const uint8_t *tx, uint32_t len)
{
	return AX5043_OK;
}

/**
 * Reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
ax5043_spi_rx(uint8_t *rx, uint32_t len)
{
	return AX5043_OK;
}

/**
 * Write data to the IC using the SPI
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to transmit
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
ax5043_spi_tx(const uint8_t *tx, uint32_t len)
{
	return AX5043_OK;
}

/**
 * Delay the execution with microsecond accuracy
 * @param us the delay in microseconds
 */
__attribute__((weak)) void
ax5043_usleep(uint32_t us)
{
}

/**
 * Returns the time in milliseconds. The counter should be monotonically increasing.
 * Internally the driver handles the event of a wrap around.
 * @return the time in milliseconds
 */
__attribute__((weak)) uint32_t
ax5043_millis()
{
	return 0;
}

/**
 * Externally enables/disables the IRQ line used.
 * @note This function is used to avoid some race conditions, especially when
 * using the SPI subsystem. Some internal IRQ sources may raise an interrupt
 * before the call of the SPI transactions finishes and make available the
 * peripheral. This depending on the peripheral implementation and/or the
 * configured priorities, can cause a deadlock. The IRQ handler is called,
 * (in most cases with higher priority) and tries to access the register space
 * of the IC using the IC. However, if the peripheral is not yet available
 * (previous SPI call has not yet finished) a deadlock occurs. If the SPI
 * peripheral implementation supports timeout, no deadlock will occur but the
 * Functionality of the IRQ handler is not guaranteed.
 *
 * @param enable set 1 to enable, 0 to disable
 */
__attribute__((weak)) void
ax5043_irq_enable(bool enable)
{
	return;
}

/**
 * Before any transmission, the driver calls this user defined function.
 * This can be used to select properly a signal path by activating a corresponding
 * RF switch and/or activate an external PA
 */
__attribute__((weak)) void
ax5043_tx_pre_callback()
{
}

/**
 * This function is automatically called by the driver at the end of every
 * transmission. In case of any error during a transmission session, the
 * driver ensures that this callback is called. Therefore it can be safely
 * used to disable any external active components like RF switches and/or
 * PA.
 *
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 */
__attribute__((weak)) void
ax5043_tx_post_callback()
{
}

/**
 * Callback that is called when a frame is received completely.
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 *
 * @param pdu pointer to the received frame
 * @param len the number of byte received
 */
__attribute__((weak)) void
ax5043_rx_complete_callback(const uint8_t *pdu, size_t len)
{
}

/**
 * Sets the power mode of the AX5043
 * @param conf the AX5043 configuration handler
 * @param mode the power mode
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_power_mode(struct ax5043_conf *conf, power_mode_t mode)
{
	int ret;
	uint8_t val;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/* Read the contents of the register */
	ret = spi_read_8(conf, &val, AX5043_REG_PWRMODE);
	if (ret) {
		return ret;
	}

	/* Keep REFEN and XOEN values */
	val &= (BIT(6) | BIT(5));

	switch (mode) {
		case POWERDOWN:
		case DEEPSLEEP:
		case STANDBY:
		case FIFO_ENABLED:
		case SYNTHRX:
		case FULLRX:
		case WOR:
		case SYNTHTX:
		case FULLTX:
			val |= mode;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	ret = spi_write_8(conf, AX5043_REG_PWRMODE, val);
	if (ret) {
		return ret;
	}
	conf->priv.pwr_mode = mode;
	return AX5043_OK;
}

/**
 * Set the TX deviation
 * @param conf the AX5043 configuration handler
 * @param mod_index the modulation index
 * @param baud the baud rate
 * @return 0 on success or negative error code
 */
static int
set_fsk_deviation(struct ax5043_conf *conf, float mod_index, uint32_t baud)
{
	int ret = AX5043_OK;
	uint32_t val;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	float f_deviation = ((mod_index / 2.0) * (float) baud);
	val = ((uint32_t)((f_deviation / conf->xtal.freq) * (1 << 24))) | 0x1;
	ret = spi_write_24(conf, AX5043_REG_FSKDEV2, val);
	if (ret) {
		return ret;
	}
	return AX5043_OK;
}


int
ax5043_tx_conf_valid(struct ax5043_conf *conf, bool *valid)
{
	if (!conf || !valid) {
		return -AX5043_INVALID_PARAM;
	}
	*valid = 0;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	*valid = conf->priv.tx_valid;
	return AX5043_OK;
}

/**
 * Sets the modulation by altering the MODULATION register
 * @param conf the AX5043 configuration handler
 * @param mod the modulation to set
 * @return 0 on success or negative error code
 */
static int
set_modulation(struct ax5043_conf *conf, modulation_t mod)
{
	int ret;
	switch (mod) {
		case ASK:
		case ASK_COHERENT:
		case PSK:
		case OQPSK:
		case MSK:
		case FSK:
		case FSK_4:
		case AFSK:
		case FM:
			ret = spi_write_8(conf, AX5043_REG_MODULATION, mod);
			if (ret) {
				return ret;
			}
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	return AX5043_OK;
}

/**
 * Sets the ENCODING register
 * @param  conf the AX5043 configuration handler
 * @param enc the structure with the different encoding parameters
 * @return 0 on success or negative error code
 */
static int
set_encoding(struct ax5043_conf *conf, const struct encoding *enc)
{
	uint8_t val = (enc->nosync << 4) | (enc->manch << 3) | (enc->scrambler << 2)
	              | (enc->diff << 1) | enc->inv;
	return spi_write_8(conf, AX5043_REG_ENCODING, val);
}

/**
 * Sets the FRAMING register and initializes the CRC seed if used
 * @param conf
 * @param framing
 * @param crc
 * @return
 */
static int
set_framing(struct ax5043_conf *conf, framing_t framing,
            const struct crc *crc)
{
	int ret = AX5043_OK;
	uint8_t val8 = 0;

	/* Check framing for validity */
	switch (framing) {
		case RAW:
		case RAW_SOFT:
		case HDLC:
		case RAW_PATTERN_MATCH:
		case M_BUS:
		case MBUS_4_6:
			val8 = framing << 1;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	/*Check validity of CRC */
	switch (crc->mode) {
		case CRC_OFF:
		case CRC16_CCITT:
		case CRC16:
		case CRC16_DNP:
		case CRC32:
			val8 |= (crc->mode << 4);
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	/* Apply framing and CRC method */
	ret = spi_write_8(conf, AX5043_REG_FRAMING, val8);
	if (ret) {
		return ret;
	}
	/* Init CRC */
	if (crc->mode != CRC_OFF) {
		ret = spi_write_32(conf, AX5043_REG_CRCINIT3, crc->init);
		if (ret) {
			return ret;
		}
	}
	return ret;
}

static int
set_packet_format(struct ax5043_conf *conf, const struct packet_format *pkt)
{
	int ret = AX5043_OK;
	uint8_t val8 = 0x0;
	val8 = pkt->addr_pos | (pkt->fec_sync_dis << 5) | (pkt->crc_skip_first << 6)
	       | (pkt->msb_first << 7);
	ret = spi_write_8(conf, AX5043_REG_PKTADDRCFG, val8);
	if (ret) {
		return ret;
	}

	val8 = pkt->len_pos | (pkt->len_bits << 4);
	ret = spi_write_8(conf, AX5043_REG_PKTLENCFG, val8);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_PKTLENOFFSET, pkt->len_offset);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_PKTMAXLEN, pkt->max_len);
	if (ret) {
		return ret;
	}

	ret = spi_write_32(conf, AX5043_REG_PKTADDR3, pkt->addr);
	if (ret) {
		return ret;
	}

	ret = spi_write_32(conf, AX5043_REG_PKTADDRMASK3, pkt->addr_mask);
	if (ret) {
		return ret;
	}
	return ret;
}

static int
set_tx_framing(struct ax5043_conf *conf, const struct tx_params *params)
{
	int ret = AX5043_OK;

	struct crc crc = {.mode = CRC_OFF, .init = 0xFFFFFFFF };
	switch (params->framing) {
		case HDLC: {
			crc.mode = CRC16_CCITT;
			crc.init = 0xFFFFFFFF;
			struct encoding enc;
			memset(&enc, 0, sizeof(struct encoding));

			/* NRZ and NRZI cannot be set at the same time */
			if (params->hdlc.en_nrz && params->hdlc.en_nrzi) {
				return -AX5043_HDLC_INVALID_PARAM;
			}
			enc.diff = params->hdlc.en_nrzi & 0x1;
			enc.inv = params->hdlc.en_nrzi & 0x1;
			enc.scrambler = params->hdlc.en_scrambler & 0x1;
			ret = set_encoding(conf, &enc);
			if (ret) {
				return ret;
			}
			if (params->hdlc.preamble_len < 1 || params->hdlc.postamble_len < 1) {
				return -AX5043_HDLC_INVALID_PARAM;
			}
			/*
			 * If scrambler is enabled, the HDLC should be repeated at least
			 * 8 times to allow the G3RUH self-synchronizing scrambler to settle
			 * at the receiver
			 */
			if (params->hdlc.en_scrambler && params->hdlc.preamble_len < 8) {
				return -AX5043_HDLC_INVALID_PARAM;
			}
			/*
			* HDLC frames should be transmitted in lsb-first order
			* so we create a new packet_format struct in order to
			* set the msb_first bit
			*/
			struct packet_format pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 0,
				.msb_first = 0,
				.len_pos = 0,
				.len_bits = 0,
				.len_offset = 0,
				.max_len = 200,
				.addr = 0,
				.addr_mask = 0x0
			};
			/* Set the packet format */
			set_packet_format(conf, &pkt);
		}
		break;
		case RAW_PATTERN_MATCH: {
			const struct pattern_framing_tx *pf = &params->pattern;
			if (pf->preamble_len > 2040 || pf->sync_len > 32) {
				return -AX5043_INVALID_PARAM;
			}

			crc = pf->crc;
			/* Apply frame encoding */
			ret = set_encoding(conf, &pf->enc);
			if (ret) {
				return ret;
			}

			/* Set the packet format */
			ret = set_packet_format(conf, &pf->pkt);
			if (ret) {
				return ret;
			}
		}
		break;
		case RAW_SOFT:
			ret = spi_write_8(conf, 0xF72, 0x06);
			if (ret) {
				return ret;
			}
			break;
		case RAW: {
			const struct raw_framing *r = &params->raw;
			crc = r->crc;
			/* Apply frame encoding */
			ret = set_encoding(conf, &r->enc);
			if (ret) {
				return ret;
			}
		}
		break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	/* Apply configuration to the FRAMING and CRCINIT registers */
	ret = set_framing(conf, params->framing, &crc);
	return ret;
}

/**
 * Configures the TX related parameters using the \ref struct tx_params.
 * This function performs extensive checks for the validity of the given
 * parameters. If the configuration is valid, it keeps
 *
 * @param conf the AX5043 configuration handler
 * @param params the TX parameters
 * @return 0 on success or negative error code
 */
int
ax5043_conf_tx(struct ax5043_conf *conf, const struct tx_params *params)
{
	int ret = AX5043_OK;
	uint8_t val8 = 0;

	if (!conf || !params) {
		return -AX5043_INVALID_PARAM;
	}
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/* Invalidate internal validity flag */
	conf->priv.tx_valid = 0;
	switch (params->rf_out_mode) {
		case TXDIFF:
			val8 = BIT(0);
			break;
		case TXSE:
			val8 = BIT(1);
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	switch (params->shaping) {
		case UNSHAPED:
		case RC:
			val8 |= (params->shaping << 2);
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	/* Enable explicitly PTTLCK and BROWN gates */
	/* FIXME! */
	/* val8 |= BIT(6) | BIT(7); */
	ret = spi_write_8(conf, AX5043_REG_MODCFGA, val8);
	if (ret) {
		return ret;
	}

	ret = set_modulation(conf, params->mod);
	if (ret) {
		return ret;
	}



	/* Apply framing and CRC */
	ret = set_tx_framing(conf, params);
	if (ret) {
		return ret;
	}

	/* Set TX power, it can re-adjusted later */
	ret = ax5043_set_tx_power(conf, params->pout_dBm);
	if (ret) {
		return ret;
	}

	/* Set baudrate */
	ret = ax5043_set_tx_baud(conf, params->baudrate);
	if (ret) {
		return ret;
	}

	/* In case of FSK/MSK , set the frequency deviation and the shaping filter */
	if (params->mod == FSK || params->mod == MSK) {
		freq_shaping_t freq_shape;
		float mod_index = 0;
		if (params->mod == FSK) {
			freq_shape = params->fsk.freq_shaping;
			mod_index = params->fsk.mod_index;
		} else if (params->mod == MSK) {
			freq_shape = params->msk.freq_shaping;
			mod_index = 0.5;
		} else {
			freq_shape = INVALID;
		}
		switch (freq_shape) {
			case EXTERNAL_FILTER:
			case GAUSIAN_BT_0_3:
			case GAUSIAN_BT_0_5:
				val8 = freq_shape;
				ret = spi_write_8(conf, AX5043_REG_MODCFGF, val8);
				if (ret) {
					return ret;
				}
				break;
			case INVALID:
			default:
				return -AX5043_INVALID_PARAM;
		}
		ret = set_fsk_deviation(conf, mod_index, params->baudrate);
		if (ret) {
			return ret;
		}
	} else if (params->mod == PSK) {
		/* Disable frequency filtering for PSK */
		ret = spi_write_8(conf, AX5043_REG_MODCFGF, 0x0);
		if (ret) {
			return ret;
		}

		ret = spi_write_24(conf, AX5043_REG_FSKDEV2, 0x0);
		if (ret) {
			return ret;
		}
		/* Undocumented register... Got it from RadioLab */
		ret = spi_write_8(conf, AX5043_REG_MODCFGP, 0xE1);
		if (ret) {
			return ret;
		}
	} else if (params->mod == OQPSK) {
		ret = set_fsk_deviation(conf, 0.5, params->baudrate);
		if (ret) {
			return ret;
		}
	}
	/* Everything is ok, copy internally and mark as valid the configuration */
	conf->priv.tx_valid = 1;
	memcpy(&conf->priv.tx, params, sizeof(struct tx_params));
	return AX5043_OK;
}


/**
 * Reconfigures the TX power. TX power settings are clamped in the [-10 - +15] dBm
 * range automatically.
 *
 * @param conf the AX5043 configuration handler
 * @param pout the desired output power in dBm
 * @return 0 on success or negative error code
 */
int
ax5043_set_tx_power(struct ax5043_conf *conf, int pout)
{
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/* Clamp power value */
	pout = min(pout, 15);
	pout = max(pout, -10);
	return spi_write_16(conf, AX5043_REG_TXPWRCOEFFB1,
	                    g_tx_gain_lookup_table[25 - (10 + pout)]);
}


/**
 * Set the TX baudrate
 * @param conf the AX5043 configuration handler
 * @param baud the baudrate
 * @return 0 on success or negative error code
 */
int
ax5043_set_tx_baud(struct ax5043_conf *conf, uint32_t baud)
{
	int ret = AX5043_OK;
	uint32_t val;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/*FIXME: Sanity check? */
	val = ((uint32_t)((baud / conf->xtal.freq) * (1 << 24))) | 0x1;
	ret = spi_write_24(conf, AX5043_REG_TXRATE2, val);
	if (ret) {
		return ret;
	}
	return AX5043_OK;
}


static int
set_rx_fsk(struct ax5043_conf *conf, const struct rx_params *params)
{
	int ret = AX5043_OK;
	/*
	 * Disable receiver frequency deviation at the initial locking as recommended
	 * by the programming manual
	 */
	ret = spi_write_16(conf, AX5043_REG_FREQDEV10, 0x0);
	if (ret) {
		return ret;
	}
	uint16_t val16 = 2 * params->fsk.mod_index * (1 << 8) * 0.8;
	ret = spi_write_16(conf, AX5043_REG_FREQDEV11, val16);
	if (ret) {
		return ret;
	}
	ret = spi_write_16(conf, AX5043_REG_FREQDEV12, val16);
	if (ret) {
		return ret;
	}
	ret = spi_write_16(conf, AX5043_REG_FREQDEV13, val16);
	return ret;
}


static int
set_rx_fsk4(struct ax5043_conf *conf, const struct rx_params *params)
{
	int ret = AX5043_OK;
	/*
	 * Disable receiver frequency deviation at the initial locking as recommended
	 * by the programming manual
	 */
	ret = spi_write_16(conf, AX5043_REG_FREQDEV10, 0x0);
	if (ret) {
		return ret;
	}
	uint16_t val16 = 2 * params->fsk.mod_index * (1 << 8) * 0.8;
	ret = spi_write_16(conf, AX5043_REG_FREQDEV11, val16);
	if (ret) {
		return ret;
	}
	ret = spi_write_16(conf, AX5043_REG_FREQDEV12, val16);
	if (ret) {
		return ret;
	}
	ret = spi_write_16(conf, AX5043_REG_FREQDEV13, val16);
	if (ret) {
		return ret;
	}
	if (params->fsk4.manual_mode) {
		ret = spi_write_8(conf, AX5043_REG_FOURFSK0,
		                  params->fsk4.leakage);
		if (ret) {
			return ret;
		}
		ret = spi_write_8(conf, AX5043_REG_FOURFSK1,
		                  params->fsk4.leakage);
		if (ret) {
			return ret;
		}
		ret = spi_write_8(conf, AX5043_REG_FOURFSK2,
		                  params->fsk4.leakage);
		if (ret) {
			return ret;
		}
		ret = spi_write_8(conf, AX5043_REG_FOURFSK3,
		                  params->fsk4.leakage);
		if (ret) {
			return ret;
		}

		uint16_t d = 3 * 512 * params->fsk4.max_deviation / params->baudrate;
		ret = spi_write_16(conf, AX5043_REG_FSKDMAX1, d);
		if (ret) {
			return ret;
		}
		d = -3 * 512 * params->fsk4.min_deviation / params->baudrate;
		ret = spi_write_16(conf, AX5043_REG_FSKDMIN1, d);
		if (ret) {
			return ret;
		}
	}
	return AX5043_OK;
}

static uint8_t
tmg_dec2exp(uint32_t x)
{
	uint32_t m = 1;
	uint32_t e = 0;
	uint32_t dist = x;
	for (uint32_t i = 0; i < 8; i++) {
		for (uint32_t j = 1; j <= 0x1FF; j++) {
			if (j * (1 << i) <= x) {
				uint32_t tmp = x - j * (1 << i);
				if (tmp < dist) {
					dist = tmp;
					e = i;
					m = j;
				}
			}
		}
	}
	return (e << 5) | m;
}

static uint8_t
gain_dec2exp(uint32_t x)
{
	uint32_t m = 1;
	uint32_t e = 0;
	uint32_t dist = x;
	for (uint32_t i = 0; i < 16; i++) {
		for (uint32_t j = 1; j <= 0xF; j++) {
			if (j * (1 << i) <= x) {
				uint32_t tmp = x - j * (1 << i);
				if (tmp < dist) {
					dist = tmp;
					e = i;
					m = j;
				}
			}
		}
	}
	return (m << 4) | e;
}

static int
set_rx_raw_pattern_match(struct ax5043_conf *conf,
                         const struct rx_params *params)
{
	const struct pattern_framing_rx *pf = &params->raw_pattern;
	if (pf->sync_len < 1 || pf->sync_len > 32) {
		return -AX5043_INVALID_PARAM;
	}
	/*
	 * All patterns are requested from the user to be in MSB first order.
	 * However, the IC needs them to be LSB first and MSB aligned in case
	 * of the pattern is smaller than the corresponding register
	 *
	 * In addition, MATCH1PAT1 is a 16-bit register. The
	 * transmitter preamble should be of course far bigger.
	 * We configure the IC with the 16-bit portion and
	 * we use the actual preamble length to tune the AGC
	 * and the state transition timeouts
	 */

	uint32_t p = (reverse_bits(pf->preamble) >> 16) << (16 - min(pf->preamble_len,
	                16));
	int ret = spi_write_16(conf, AX5043_REG_MATCH1PAT1, p);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_MATCH1LEN,
	                  ((pf->preamble_unencoded & 0x1) << 7) | (pf->preamble_len - 1));
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_MATCH1MAX, pf->preamble_max);
	if (ret) {
		return ret;
	}

	/*
	 * Pattern for Match Unit 0.
	 * The AX5043 driver requests  the sync word in MSB first order.
	 * However, the IC needs them LSB first. In addition, patterns of
	 * length less than 32 must be MSB aligned
	 */
	p = reverse_bits(pf->sync) << (32 - pf->sync_len);
	ret = spi_write_32(conf, AX5043_REG_MATCH0PAT3, p);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_MATCH0LEN,
	                  ((pf->sync_unencoded & 0x1) << 7) | (pf->sync_len - 1));
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_MATCH0MAX, pf->sync_max);
	if (ret) {
		return ret;
	}

	/* Apply frame encoding */
	ret = set_encoding(conf, &pf->enc);
	if (ret) {
		return ret;
	}

	/* Set the packet format */
	ret = set_packet_format(conf, &pf->pkt);
	if (ret) {
		return ret;
	}

	/* Apply configuration to the FRAMING and CRCINIT registers */
	ret = set_framing(conf, RAW_PATTERN_MATCH, &pf->crc);
	if (ret) {
		return ret;
	}

	/* Tune AGC and timeouts  */
	const uint32_t preamble_len1 = pf->preamble_len / 4;
	if (preamble_len1) {
		uint8_t tmp = tmg_dec2exp(preamble_len1);
		ret = spi_write_8(conf, AX5043_REG_TMGRXPREAMBLE1, tmp);
		if (ret) {
			return ret;
		}
	} else {
		ret = spi_write_8(conf, AX5043_REG_TMGRXPREAMBLE1, 0x0);
		if (ret) {
			return ret;
		}
	}
	uint8_t tmp = tmg_dec2exp(pf->preamble_len + pf->sync_len);
	ret = spi_write_8(conf, AX5043_REG_TMGRXPREAMBLE2, tmp);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_TMGRXPREAMBLE3, tmp);
	if (ret) {
		return ret;
	}

	/*
	 * Set the RX parameters.
	 * Use different settings for the initial settling, PATTERN0 and during
	 * packet reception on the registers set 0, 1 and 3 respectively
	 */
	ret = spi_write_8(conf, AX5043_REG_RXPARAMSETS, 0xF4);
	if (ret) {
		return ret;
	}

	float attack;
	float decay;
	if (params->mod == ASK || params->mod == ASK_COHERENT) {
		/* Calculate the attack rate*/
		float c = cosf((1 << 5) * PI * conf->priv.f_xtaldiv * (params->baudrate / 10) /
		               conf->xtal.freq);
		attack = -log2f(1 - c + sqrtf(c * c) - 4 * c + 3);
		/* Calculate the decay rate*/
		c = cosf((1 << 5) * PI * conf->priv.f_xtaldiv * (params->baudrate / 100) /
		         conf->xtal.freq);
		decay = -log2f(1 - c + sqrtf(c * c) - 4 * c + 3);

	} else {
		float c = cosf((1 << 5) * PI * conf->priv.f_xtaldiv * params->baudrate /
		               conf->xtal.freq);
		/* Calculate the attack rate*/
		attack = -log2f(1 - c + sqrtf(c * c) - 4 * c + 3);
		/* Calculate the decay rate*/
		c = cosf((1 << 5) * PI * conf->priv.f_xtaldiv * (params->baudrate / 10) /
		         conf->xtal.freq);
		decay = -log2f(1 - c + sqrtf(c * c) - 4 * c + 3);

	}
	uint8_t agc = (min((uint8_t) decay, 0xF) << 4) | min((uint8_t) attack, 0xF);


	/* Use the same bitrate-based setting for both AGC0 and AGC1*/
	ret = spi_write_8(conf, AX5043_REG_AGCGAIN0, agc);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_AGCGAIN1, agc);
	if (ret) {
		return ret;
	}

	/* Freeze AGC after the pattern 0 is matched */
	ret = spi_write_8(conf, AX5043_REG_AGCGAIN3, 0xFF);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_AGCTARGET0, 0x84);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_AGCTARGET1, 0x84);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_AGCTARGET2, 0x84);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_AGCTARGET3, 0x84);
	if (ret) {
		return ret;
	}



	/* Time gain tuning */
	uint8_t timegain = gain_dec2exp(conf->priv.rxdatarate / 4);
	ret = spi_write_8(conf, AX5043_REG_TIMEGAIN0, timegain);
	if (ret) {
		return ret;
	}

	/*
	 * After some experimentation, it seems that TIMEGAIN1 and TIMEGAIN3
	 * should be TIMEGAIN0/4 and TIMEGAIN0/8 respectively
	 */
	uint32_t dec = (timegain >> 4) * (1 << (timegain & 0xF));
	ret = spi_write_8(conf, AX5043_REG_TIMEGAIN1, gain_dec2exp(dec / 4));
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, AX5043_REG_TIMEGAIN3, gain_dec2exp(dec / 8));
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_TMGRXCOARSEAGC, 0x7F);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_TMGRXRSSI, 0x3);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_TMGRXSETTLE, 0x31);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_TMGRXBOOST, 0x3E);
	if (ret) {
		return ret;
	}

	return AX5043_OK;
}

static int
set_rx_framing(struct ax5043_conf *conf, const struct rx_params *params)
{
	int ret = AX5043_OK;
	switch (params->framing) {
		case RAW_PATTERN_MATCH:
			ret = set_rx_raw_pattern_match(conf, params);
			if (ret) {
				return ret;
			}
			break;
		case RAW_SOFT: {
			struct crc crc = {.mode = CRC_OFF, .init = 0xFFFFFFFF };
			ret = spi_write_8(conf, 0xF72, 0x06);
			if (ret) {
				return ret;
			}
			ret = set_framing(conf, params->framing, &crc);
		}
		break;
		case HDLC:
		case M_BUS:
		case MBUS_4_6:
			return -AX5043_NOT_IMPLEMENTED;
		default:
			return -AX5043_INVALID_PARAM;
	}
	return ret;
}

/**
 * Checks if the RX configuration is valid. The configuration is valid
 * if the last call of the ax5043_conf_rx() was successful
 *
 * @param conf the AX5043 configuration handler
 * @param valid pointer to hold the validity result
 * @return 0 on success or negative error code
 */
int
ax5043_rx_conf_valid(struct ax5043_conf *conf, bool *valid)
{
	if (!conf || !valid) {
		return -AX5043_INVALID_PARAM;
	}
	*valid = 0;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	*valid = conf->priv.rx_valid;
	return AX5043_OK;
}

/**
 * Decimation is another undocumented register. It depends on the desired
 * RX bandwidth and specified the actual 3dB receiver bandwidth
 * @param conf the AX5043 configuration handler
 * @param bw the desired RX bandwidth
 * @return 0 on success or negative error code
 */
static int
set_decimation(struct ax5043_conf *conf, float bw)
{
	for (uint8_t dec = 2; dec < 0x7F; dec++) {
		/*
		 * Try to find the best decimation value based on the user specified
		 * bandwidth and the actual filter bandwidth. After playing around with
		 * the RadioLab, it seems that the user defined filter bandwidth should
		 * be less that 2.727385 kHz from the baseband frequency.
		 *
		 * Baseband frequency is also not documented. After some experimentation
		 * we found out that the baseband frequency should be around
		 * 4.514733 times the 3 dB RX bandwidth
		 *
		 */
		if (conf->xtal.freq / ((1 << 4) * conf->priv.f_xtaldiv * dec) <
		    (bw + 2.7273850e3f) * 4.514733f) {
			conf->priv.decimation = dec;
			int ret = spi_write_8(conf, AX5043_REG_DECIMATION, dec);
			if (ret) {
				return ret;
			}
			/* Calculate the passband frequency */
			conf->priv.bb_freq = conf->xtal.freq
			                     / (conf->priv.decimation * (1 << 4) * conf->priv.f_xtaldiv);

			conf->priv.rx_bw_3db = conf->priv.bb_freq / 4.514733f;
			return AX5043_OK;
		}
	}
	/* Probably a setting is not correct */
	return -AX5043_INVALID_CONF;
}

/**
 * IFFREQ is a register depending on the IF frequency value. Again this is
 * another undocumented detail of the IC. After 4 attempts with the ON support
 * center, still now clue about how this value is calculated.
 * @param conf the AX5043 configuration handler
 * @param params the RX configuration
 * @return 0 on success or negative error code
 */
static int
set_iffreq(struct ax5043_conf *conf, const struct rx_params *params)
{
	/* Some info from https://github.com/richardeoin/ax/ */
	switch (params->mod) {
		case FSK:
		case MSK:
		case FSK_4:
			conf->priv.if_freq = (4.514733 * params->bandwidth) / 6;
			if (conf->priv.if_freq < 3180) {
				conf->priv.if_freq = 3180; /* minimum 3180 Hz */
			}
			uint16_t val = (conf->priv.if_freq * conf->priv.f_xtaldiv / conf->xtal.freq) *
			               (1 << 20);
			val |= 0x1;
			int ret = spi_write_16(conf, AX5043_REG_IFFREQ1, val);
			if (ret) {
				return ret;
			}
			conf->priv.iffreq = val;
			return AX5043_OK;
		default:
			return -AX5043_NOT_IMPLEMENTED;
	}
	return -AX5043_NOT_IMPLEMENTED;
}

int
ax5043_conf_rx(struct ax5043_conf *conf, const struct rx_params *params)
{
	int ret = AX5043_OK;
	uint32_t val = 0;

	if (!conf || !params) {
		return -AX5043_INVALID_PARAM;
	}
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/* Invalidate internal validity flag */
	conf->priv.rx_valid = 0;

	ret = set_decimation(conf, params->bandwidth);
	if (ret) {
		return ret;
	}

	ret = set_iffreq(conf, params);
	if (ret) {
		return ret;
	}

	/* Apply RX data rate */
	val = (uint32_t)(((1 << 7) * conf->xtal.freq) / (conf->priv.f_xtaldiv *
	                 params->baudrate * conf->priv.decimation)) | 0x1;
	conf->priv.rxdatarate = val;
	ret = spi_write_24(conf, AX5043_REG_RXDATARATE2, val);
	if (ret) {
		return ret;
	}
	/* TODO: Ensure RXDATARATE - TIMEGAINx ≥ 2^12 */

	/*
	 * Set the maximum allowed data rate offset. If this is less than 1%
	 * the data rate recovery mechanism is disabled
	 */
	if (((float)params->dr_offset) / params->baudrate < 0.01) {
		val = 0x0;
	} else {
		val = (uint32_t)(((1 << 7) * conf->xtal.freq * params->dr_offset) /
		                 (conf->priv.f_xtaldiv * params->baudrate * params->baudrate *
		                  conf->priv.decimation)) | 0x1;
	}
	conf->priv.maxdroffset = val;
	ret = spi_write_24(conf, AX5043_REG_MAXDROFFSET2, val);
	if (ret) {
		return ret;
	}

	/* Set the maximum allowed RF carrier offset and where to apply correction */
	switch (params->freq_offset_corr) {
		case SECOND_LO:
		case FIRST_LO:
			val = params->freq_offset_corr << 23;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	float offset = params->max_rf_offset;
	val |= ((uint32_t)(offset / conf->xtal.freq * (1 << 24)) & ((1 << 20) - 1));
	val |= 0x1;
	conf->priv.maxrfoffset = val;
	ret = spi_write_24(conf, AX5043_REG_MAXRFOFFSET2, val);
	if (ret) {
		return ret;
	}

	/* Bypass amplitude filter */
	ret = spi_write_8(conf, AX5043_REG_AMPLFILTER, 0x0);
	if (ret) {
		return ret;
	}

	/* Disable leakage on the frequency recovery loop */
	ret = spi_write_8(conf, AX5043_REG_FREQUENCYLEAK, 0x0);
	if (ret) {
		return ret;
	}

	/* Apply modulation specific parameterization */
	switch (params->mod) {
		case  FSK:
			ret = set_rx_fsk(conf, params);
			break;
		case FSK_4:
			ret = set_rx_fsk4(conf, params);
			break;
		default:
			break;
	}
	ret = set_modulation(conf, params->mod);
	if (ret) {
		return ret;
	}

	/* Diversity or antenna selection */
	ret = spi_write_8(conf, AX5043_REG_DIVERSITY,
	                  (params->en_diversity & 0x1) | ((params->antesel & 0x1) << 1));
	if (ret) {
		return ret;
	}

	/* Set the performance registers for RX */
	ret = spi_write_8(conf, 0xF21, 0x5C);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, 0xF22, 0x53);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, 0xF23, 0x76);
	if (ret) {
		return ret;
	}
	ret = spi_write_8(conf, 0xF26, 0x92);
	if (ret) {
		return ret;
	}
	/* In case of Raw soft bits, this will be set from set_rx_framing() */
	ret = spi_write_8(conf, 0xF72, 0x00);
	if (ret) {
		return ret;
	}

	/* Yet another undocumented register */
	ret = spi_write_8(conf, 0xF18, 0x02);
	if (ret) {
		return ret;
	}

	/* Apply framing specific parameterization */
	ret = set_rx_framing(conf, params);
	if (ret) {
		return ret;
	}

	memcpy(&conf->priv.rx, params, sizeof(struct rx_params));
	conf->priv.rx_valid = 1;
	return ret;
}

/**
 * Starts the IC RX procedure. This includes setting the state of the IC
 * in FULLRX and enabling the appropriate interrupt signals. This function
 * does not receive any data. Received data are available through the
 * rx_complete_clbk that has been declared during the ax5043_init() function.
 * @note This function requires a valid RX configuration applied using the
 * ax5043_conf_rx().
 *
 * @param conf the AX5043 configuration handler
 * @return 0 on success or negative error code
 */
int
ax5043_start_rx(struct ax5043_conf *conf)
{
	int ret = AX5043_OK;
	bool valid;

	/* Check if the user has configured properly the RX */
	ret = ax5043_rx_conf_valid(conf, &valid);
	if (ret) {
		return ret;
	}
	if (!valid) {
		return -AX5043_RX_INVALID;
	}

	/* Reset internal state */
	memset(conf->rx_buffer, 0, conf->rx_buffer_len);
	conf->priv.rx_buffer_idx = 0;

	ret = ax5043_set_power_mode(conf, FULLRX);
	if (ret) {
		/* Try to power the IC down */
		ax5043_set_power_mode(conf, POWERDOWN);
		return ret;
	}

	wait_xtal(conf, 100);

	/* Wait for the FIFO to become ready */
	uint8_t val = 0;
	while (!val) {
		spi_read_8(conf, &val, AX5043_REG_POWSTAT);
		/* Select only the modem power state */
		val &= AX5043_SVMODEM;
	}

	/* Enable the necessary IRQ sources for the RX */
	ax5043_irq_enable(0);
	ret = spi_write_16(conf, AX5043_REG_IRQMASK1,
	                   AX5043_IRQMFIFONOTEMPTY);
	if (ret) {
		ax5043_set_power_mode(conf, POWERDOWN);
		return ret;
	}

	ret = spi_write_8(conf, AX5043_REG_RADIOEVENTMASK0, AX5043_REVMDONE);
	if (ret) {
		ax5043_set_power_mode(conf, POWERDOWN);
		return ret;
	}
	ax5043_irq_enable(1);
	return AX5043_OK;
}

/**
 * Starts the IC RX procedure. This includes setting the state of the IC
 * in POWERDOWN and disabling all interrupt signals set by ax5043_start_rx().
 *
 * @param conf the AX5043 configuration handler
 * @param timeout_ms the amount of milliseconds to wait until stopping the RX by
 * force
 * @return 0 on success or negative error code
 */
int
ax5043_stop_rx(struct ax5043_conf *conf, uint32_t timeout_ms)
{
	int ret = AX5043_OK;
	/* Disable any IRQ source */
	spi_write_16(conf, AX5043_REG_IRQMASK1, 0x0);
	spi_write_8(conf, AX5043_REG_RADIOEVENTMASK0, 0x0);
	/*
	 * Before going to POWEDOWN, set the IC to standby and empty any
	 * leftover bytes in the queue
	 */
	ax5043_set_power_mode(conf, STANDBY);
	uint16_t cnt = 0;
	ret = spi_read_16(conf, &cnt, AX5043_REG_FIFOCOUNT1);
	if (ret) {
		ax5043_set_power_mode(conf, POWERDOWN);
		return ret;
	}
	uint32_t start = ax5043_millis();
	while (cnt) {
		uint8_t tmp;
		ret = spi_read_8(conf, &tmp, AX5043_REG_FIFODATA);
		if (ret) {
			ax5043_set_power_mode(conf, POWERDOWN);
			return ret;
		}
		ret = spi_read_16(conf, &cnt, AX5043_REG_FIFOCOUNT1);
		if (ret) {
			ax5043_set_power_mode(conf, POWERDOWN);
			return ret;
		}
		if (ax5043_millis() - start > timeout_ms) {
			ax5043_set_power_mode(conf, POWERDOWN);
			return -AX5043_TIMEOUT;
		}
	}
	return ax5043_set_power_mode(conf, POWERDOWN);
}

/**
 * Update the status of the receiver, if the IC is on FULLRX mode
 *
 * @param conf the AX5043 configuration handler
 * @return 0 on success or negative error code
 */
int
ax5043_update_rx_status(struct ax5043_conf *conf)
{
	int ret = AX5043_OK;
	uint8_t val8 = 0;
	uint16_t val16 = 0;
#if 0
	uint32_t val32 = 0;
#endif
	struct rx_status *status = &conf->rx_status;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	if (conf->priv.pwr_mode != FULLRX) {
		return AX5043_NOT_READY;
	}
#if 0
	/* Get RSSI */
	ret = ax5043_rssi(conf, &status->rssi, &status.bgnrssi);
	if (ret) {
		return ret;
	}
#endif
	/* Get AGC status */
	ret = spi_read_8(conf, &val8, AX5043_REG_AGCCOUNTER);
	if (ret) {
		return ret;
	}
	status->agc = val8 * 0.75f;
#if 0
	/* Data rate tracking */
	ret = spi_read_24(conf, &val32, AX5043_REG_TRKDATARATE2);
	if (ret) {
		return ret;
	}
	status->datarate_tracking = val32;
#endif

	/* Amplitude tracking */
	ret = spi_read_16(conf, &status->amplitude_tracking, AX5043_REG_TRKAMPL1);
	if (ret) {
		return ret;
	}

	/* Phase tracking */
	ret = spi_read_16(conf, &val16, AX5043_REG_TRKPHASE1);
	if (ret) {
		return ret;
	}
	status->phase_tracking = val16;
#if 0
	/* RF frequency tracking */
	ret = spi_read_24(conf, &val32, AX5043_REG_TRKRFFREQ2);
	if (ret) {
		return ret;
	}
	status->rf_freq_tracking = val32;

	/* Frequency tracking */
	ret = spi_read_16(conf, &status->freq_tracking, AX5043_REG_TRKFREQ1);
	if (ret) {
		return ret;
	}
#endif

	ret = spi_read_8(ax5043_conf, &status->radio_state, AX5043_REG_RADIOSTATE);
	if (ret) {
		return ret;
	}
	return AX5043_OK;
}

/**
 * This function tunes the AX5043 in a specific frequency, after setting
 * properly the PLL parameters, based on the direction of the request.
 * (regust for RX or TX)
 * @param conf the AX5043 configuration handler
 * @param mode the frequency mode
 * @param dir the direction of the request (RX or TX)
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_tune(struct ax5043_conf *conf, freq_mode_t mode, tune_dir_t dir)
{
	int ret = AX5043_OK;
	uint32_t freq = 0;
	uint8_t rfdiv = 0;
	uint8_t pllcodediv = 0;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	struct ax5043_conf_priv *conf_priv = &conf->priv;
	switch (mode) {
		case FREQA_MODE:
			freq = conf_priv->freqa_req;
			break;
		case FREQB_MODE:
			freq = conf_priv->freqb_req;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	switch (dir) {
		case TUNE_RX:
			ret = set_rx_synth(conf, mode);
			break;
		case TUNE_TX:
			ret = set_tx_synth(conf, mode);
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	if (ret) {
		return ret;
	}

	/*
	 * Check the frequency range. The actual range depends on the VCO used.
	 * \ref ax5043_config_freq() checks for a valid frequency, but there is
	 * always the case someone accidentally to alter the private members
	 * of the configuration structure
	 */
	switch (conf->vco) {
		case VCO_INTERNAL:
			if (freq >= MIN_RF_FREQ_INT_VCO_RFDIV0
			    && freq <= MAX_RF_FREQ_INT_VCO_RFDIV0) {
				rfdiv = AX5043_RFDIV0;
			} else if (freq >= MIN_RF_FREQ_INT_VCO_RFDIV1
			           && freq <= MAX_RF_FREQ_INT_VCO_RFDIV1) {
				rfdiv = AX5043_RFDIV1;
			} else {
				return -AX5043_INVALID_PARAM;
			}
			break;
		case VCO_EXTERNAL:
			if (freq >= MIN_RF_FREQ_EXT_VCO_RFDIV0
			    && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV0) {
				rfdiv = AX5043_RFDIV0;
			} else if (freq >= MIN_RF_FREQ_EXT_VCO_RFDIV1
			           && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV1) {
				rfdiv = AX5043_RFDIV1;
			} else {
				return -AX5043_INVALID_PARAM;
			}
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	pllcodediv = rfdiv | (conf->vco << 4);
	ret = spi_write_8(conf, AX5043_REG_PLLVCODIV, pllcodediv);
	if (ret) {
		return ret;
	}

	/* Write properly the F34 performance register based on the RFDIV*/
	if (rfdiv == AX5043_RFDIV1) {
		ret = spi_write_8(conf, 0xF34, 0x28);
	} else {
		ret = spi_write_8(conf, 0xF34, 0x08);
	}
	if (ret) {
		return ret;
	}

	/*
	 * Set the RF frequency
	 * Frequency should be avoided to be a multiple integer of the crystal
	 * frequency, so we always set to 1 the LSB
	 */
	uint32_t reg_val = ((uint32_t)((freq / conf->xtal.freq) * (1 << 24))) | 0x1;
	if (mode == FREQA_MODE) {
		ret = spi_write_32(conf, AX5043_REG_FREQA3, reg_val);
		if (ret == AX5043_OK) {
			conf_priv->freqa = freq;
		}
	} else {
		ret = spi_write_32(conf, AX5043_REG_FREQB3, reg_val);
		if (ret == AX5043_OK) {
			conf_priv->freqb = freq;
		}
	}
	if (ret) {
		return ret;
	}
	/* Perform autoranging if it is necessary */
	return autoranging(conf, mode);
}

/**
 * The AX5043 IC has two different set of registers for setting the RF frequency.
 * This allows very fast tuning settling times between different frequency bands.
 * It also makes possible for altering frequency, without affecting in any way
 * the currently active frequency setup.
 *
 * @note: This function sets only the desired frequency at the corresponding
 * registers (A or B). It does not perform any actual RF tuning. Users should
 * call the \ref ax5043_tune() function for the hardware to tune on the desired
 * frequency
 *
 * @param conf the AX5043 configuration handler
 * @param mode the frequency mode (A or B) to set the frequency
 * @param freq the frequency in Hz
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_config_freq(struct ax5043_conf *conf, freq_mode_t mode, uint32_t freq)
{
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	if (mode != FREQA_MODE && mode != FREQB_MODE) {
		return -AX5043_INVALID_PARAM;
	}

	struct ax5043_conf_priv *conf_priv = &conf->priv;

	/* Check the frequency range. The actual range depends on the VCO used */
	switch (conf->vco) {
		case VCO_INTERNAL:
			if (!((freq >= MIN_RF_FREQ_INT_VCO_RFDIV0
			       && freq <= MAX_RF_FREQ_INT_VCO_RFDIV0)
			      || (freq >= MIN_RF_FREQ_INT_VCO_RFDIV1
			          && freq <= MAX_RF_FREQ_INT_VCO_RFDIV1))) {
				return -AX5043_INVALID_PARAM;
			}
			break;
		case VCO_EXTERNAL:
			if (!((freq >= MIN_RF_FREQ_EXT_VCO_RFDIV0
			       && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV0)
			      || (freq >= MIN_RF_FREQ_EXT_VCO_RFDIV1
			          && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV1))) {
				return -AX5043_INVALID_PARAM;
			}
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	switch (mode) {
		case FREQA_MODE:
			conf_priv->freqa_req = freq;
			break;
		case FREQB_MODE:
			conf_priv->freqb_req = freq;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	return AX5043_OK;
}

/**
 * Sets the TX frequency synthesizer related configuration registers.
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
set_tx_synth(struct ax5043_conf *conf, freq_mode_t mode)
{
	int ret;
	uint8_t val;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	switch (mode) {
		case FREQA_MODE:
			val = 0x0;
			break;
		case FREQB_MODE:
			val = 1 << 7;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	/* FIXME: Support parametric external filter setup */
	/* Bypass external filter and use 100 kHZ loop bandwidth */
	val |= BIT(3) | BIT(0);
	ret = spi_write_8(conf, AX5043_REG_PLLLOOP, val);
	if (ret) {
		return ret;
	}

	/* Apply the settings during BOOST mode.
	 * Bypass external filter and use 500 kHZ loop bandwidth
	 */
	val &= (1 << 7);
	val |= BIT(3) | BIT(1) | BIT(0);
	ret = spi_write_8(conf, AX5043_REG_PLLLOOPBOOST, val);
	if (ret) {
		return ret;
	}

	/*
	 * Set the charge pump current based on the loop bandwidth
	 * 68 uA @ 100 kHZ
	 */
	ret = spi_write_8(conf, AX5043_REG_PLLCPI, (uint8_t)(68 / 8.5));
	if (ret) {
		return ret;
	}

	/*
	 * Set the charge pump current based on the loop bandwidth in BOOST mode
	 * 1.7 mA @ 500 kHZ
	 */
	ret = spi_write_8(conf, AX5043_REG_PLLCPIBOOST, (uint8_t)(1700 / 8.5));
	return ret;
}


/**
 * Sets the RX frequency synthesizer related configuration registers.
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
set_rx_synth(struct ax5043_conf *conf, freq_mode_t mode)
{
	int ret;
	uint8_t val;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	switch (mode) {
		case FREQA_MODE:
			val = 0x0;
			break;
		case FREQB_MODE:
			val = 1 << 7;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	/* FIXME: Support parametric external filter setup */
	/* Bypass external filter and use 500 kHZ loop bandwidth */
	val |= BIT(3) | BIT(1) | BIT(0);
	ret = spi_write_8(conf, AX5043_REG_PLLLOOP, val);
	if (ret) {
		return ret;
	}

	/* Apply the same settings during BOOST mode.
	 */
	ret = spi_write_8(conf, AX5043_REG_PLLLOOPBOOST, val);
	if (ret) {
		return ret;
	}

	/*
	 * Set the charge pump current based on the loop bandwidth
	 * 1.7 mA @ 500 kHZ
	 */
	ret = spi_write_8(conf, AX5043_REG_PLLCPI, (uint8_t)(1700 / 8.5));
	if (ret) {
		return ret;
	}

	/*
	 * Set the charge pump current based on the loop bandwidth in BOOST mode
	 * 1.7 mA @ 500 kHZ
	 */
	ret = spi_write_8(conf, AX5043_REG_PLLCPIBOOST, (uint8_t)(1700 / 8.5));
	return ret;
}

/**
 * Sets the PLL related configuration registers.
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
set_pll_params(struct ax5043_conf *conf)
{
	int ret;
	uint8_t i = 8;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	/* Set VCO to manual */
	ret = spi_write_8(conf, AX5043_REG_PLLVCOI,
	                  AX5043_PLLVCOI_MANUAL | (1250 / 50));
	if (ret) {
		return ret;
	}

	/*
	 * According to the manual PLL ranging clock should be less than 1/10
	 * of the PLL loop bandwidth. The smallest PLL bandwidth configuration
	 * is 100 kHz.
	 */
	while ((conf->xtal.freq / (1 << i)) > 100e3) {
		i++;
	}
	i = i > 15 ? 15 : i;
	ret = spi_write_8(conf, AX5043_REG_PLLRNGCLK, i - 8);
	return ret;
}

/**
 * Performs auto-ranging using the frequency registers configured by
 * ax5043_freqsel().
 *
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
autoranging(struct ax5043_conf *conf, freq_mode_t mode)
{
	int ret = AX5043_OK;
	uint16_t pllranging_reg;
	uint8_t val = 0;
	uint32_t new_freq = 0;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	struct ax5043_conf_priv *conf_priv = &conf->priv;
	switch (mode) {
		case FREQA_MODE:
			pllranging_reg = AX5043_REG_PLLRANGINGA;
			new_freq = conf_priv->freqa;
			break;
		case FREQB_MODE:
			pllranging_reg = AX5043_REG_PLLRANGINGB;
			new_freq = conf_priv->freqb;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}

	ret = ax5043_set_power_mode(conf, STANDBY);
	if (ret) {
		return ret;
	}
	ret = wait_xtal(conf, 100);
	if (ret) {
		return ret;
	}

	/* Check if autoranging is necessary */
	if (mode == FREQA_MODE) {
		if (abs(new_freq - conf_priv->auto_rng_freqa) < 2500000) {
			return AX5043_OK;
		}
	} else {
		if (abs(new_freq - conf_priv->auto_rng_freqb) < 2500000) {
			return AX5043_OK;
		}
	}

	/* Write the initial VCO setting and start autoranging */
	val = BIT(4) | AX5043_VCOR_INIT;
	ret = spi_write_8(conf, pllranging_reg, val);
	if (ret) {
		return ret;
	}

	ax5043_usleep(10);
	val = 0;
	/* Wait until the autoranging is complete */
	while (val & BIT(4)) {
		ret = spi_read_8(conf, &val, pllranging_reg);
		if (ret) {
			return ret;
		}
	}

	if (val & BIT(5)) {
		return -AX5043_AUTORANGING_ERROR;
	}
	/* Mark the last successful autorange frequency */
	if (mode == FREQA_MODE) {
		conf_priv->auto_rng_freqa = new_freq;
	} else {
		conf_priv->auto_rng_freqb = new_freq;
	}
	return AX5043_OK;
}

static int
tx_frame_end(struct ax5043_conf *conf)
{
	int ret;
	/* Disable IRQ sources that are not needed anymore */
	tx_fifo_irq_enable(conf, 0);
	spi_write_8(conf, AX5043_REG_RADIOEVENTMASK0, 0);
	/*
	 * Wait for the FIFO to become empty.
	 * See https://gitlab.com/librespacefoundation/ax5043-driver/issues/18
	 * for details
	 */
	uint16_t fcnt = 1;
	while (fcnt) {
		spi_read_16(conf, &fcnt, AX5043_REG_FIFOCOUNT1);
	}

	/* Set AX5043 to power down mode */
	ret = ax5043_set_power_mode(conf, POWERDOWN);
	ax5043_tx_post_callback();
	g_tx_active = 0;
	return ret;
}

static int
tx_frame(struct ax5043_conf *conf, uint32_t fifo_len, uint32_t timeout_ms)
{
	int ret = AX5043_OK;

	bool single_fifo_access = 0;
	uint32_t start = ax5043_millis();

	g_tx_buf_idx = 0;
	g_tx_remaining = fifo_len;


	/*
	 * Write the first portion of the frame and let the \ref tx_irq() to do
	 * the rest
	 */
	uint32_t avail = min(AX5043_FIFO_MAX_SIZE, g_tx_remaining);
	if (g_tx_remaining == avail) {
		single_fifo_access = 1;
	} else {
		single_fifo_access = 0;
	}

	/* Set AX5043 to FULLTX mode */
	ret = ax5043_set_power_mode(conf, FULLTX);
	if (ret) {
		return ret;
	}

	wait_xtal(conf, 100);

	/* Wait for the FIFO to become ready */
	uint8_t val = 0;
	while (!val) {
		spi_read_8(conf, &val, AX5043_REG_POWSTAT);
		/* Select only the modem power state */
		val &= AX5043_SVMODEM;
		if (ax5043_millis() - start > timeout_ms) {
			ret = -AX5043_TIMEOUT;
			break;
		}
	}

	/* Enable the necessary IRQs from the RADIOSTATE register */
	ret = spi_write_8(conf, AX5043_REG_RADIOEVENTMASK0, AX5043_REVMDONE);
	if (ret) {
		return ret;
	}

	/* Fire-up the first data to the FIFO */
	ret = spi_write(conf, AX5043_REG_FIFODATA, conf->tx_buffer, avail);
	if (ret) {
		return ret;
	}
	g_tx_active = 1;
	g_tx_remaining -= avail;
	g_tx_buf_idx = avail;
	/* Commit to FIFO ! */
	ret = spi_write_8(conf, AX5043_REG_FIFOSTAT, AX5043_FIFO_COMMIT_CMD);
	/*
	 * Enable FIFO free IRQ, only if it is needed.
	 * From now on, the IRQ handler will deal with filling the FIFO properly if
	 * the frame size exceeds the maximum FIFO packet size
	 */
	if (!single_fifo_access) {
		tx_fifo_irq_enable(conf, 1);
	} else {
		tx_fifo_irq_enable(conf, 0);
	}
	return ret;
}

static uint32_t
prepare_tx_frame_raw(const struct ax5043_conf *conf, const uint8_t *in,
                     uint32_t len)
{
	uint32_t idx = 0;
	uint8_t cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, len);
	uint32_t remaining = len;

	conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
	conf->tx_buffer[idx++] = cmd_len + 1;
	if (cmd_len == len) {
		conf->tx_buffer[idx++] = AX5043_FIFO_PKTSTART | AX5043_FIFO_PKTEND;
	} else {
		conf->tx_buffer[idx++] = AX5043_FIFO_PKTSTART;
	}
	memcpy(conf->tx_buffer + idx, in, cmd_len);
	idx += cmd_len;
	remaining -= cmd_len;

	while (remaining) {
		cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, remaining);
		remaining -= cmd_len;
		conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
		conf->tx_buffer[idx++] = cmd_len + 1;
		if (remaining == 0) {
			conf->tx_buffer[idx++] = AX5043_FIFO_PKTEND;
		} else {
			conf->tx_buffer[idx++] = 0;
		}
		memcpy(conf->tx_buffer + idx, in + len - remaining - cmd_len, cmd_len);
		idx += cmd_len;
	}
	return idx;
}

static uint32_t
prepare_tx_frame(const struct ax5043_conf *conf, const uint8_t *in,
                 uint32_t len)
{
	uint32_t idx = 0;
	uint8_t cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, len);
	uint32_t remaining = len;
	const struct pattern_framing_tx *fr = &conf->priv.tx.pattern;
	uint8_t pkt_start_set = 0;

	/* Start with the preamble if any */
	if (fr->preamble_len) {
		conf->tx_buffer[idx++] = AX5043_FIFO_REPEATDATA_CMD;
		conf->tx_buffer[idx] = AX5043_FIFO_PKTSTART | AX5043_FIFO_NOCRC
		                       | AX5043_FIFO_RAW;
		pkt_start_set = 1;
		if (fr->preamble_unencoded) {
			conf->tx_buffer[idx] |= AX5043_FIFO_UNENC;
		}
		idx++;
		conf->tx_buffer[idx++] = fr->preamble_len / 8;
		conf->tx_buffer[idx++] = fr->preamble;

		/*
		 * Check if residual bits should be transmitted. This happens when
		 * the preamble is not a multiple of 8-bits
		 */
		const uint8_t residual = fr->preamble_len % 8;
		if (residual) {
			conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
			conf->tx_buffer[idx++] = 2;
			conf->tx_buffer[idx] = AX5043_FIFO_NOCRC | AX5043_FIFO_RESIDUE
			                       | AX5043_FIFO_RAW;
			if (fr->preamble_unencoded) {
				conf->tx_buffer[idx] |= AX5043_FIFO_UNENC;
			}
			idx++;
			if (fr->pkt.msb_first) {
				conf->tx_buffer[idx++] = BIT(7 - residual) |
				                         (~((BIT(8 - residual) - 1)) & fr->preamble);
			} else {
				conf->tx_buffer[idx++] = BIT(residual)
				                         | ((BIT(residual) - 1) & fr->preamble);
			}
		}
	}

	/* Do the same for the synchronization word */
	if (fr->sync_len / 8) {
		conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
		uint8_t prlen = fr->sync_len / 8;
		const uint8_t residual = fr->sync_len % 8;
		if (residual) {
			prlen++;
			conf->tx_buffer[idx++] = prlen + 1;
			conf->tx_buffer[idx] = AX5043_FIFO_NOCRC | AX5043_FIFO_RAW
			                       | AX5043_FIFO_RESIDUE;
		} else {
			conf->tx_buffer[idx++] = prlen + 1;
			conf->tx_buffer[idx] = AX5043_FIFO_NOCRC | AX5043_FIFO_RAW;
		}

		if (fr->sync_unencoded) {
			conf->tx_buffer[idx] |= AX5043_FIFO_UNENC;
		}
		/* If no preamble was used, append the PKTSTART flag*/
		if (fr->preamble_len == 0) {
			conf->tx_buffer[idx] |= AX5043_FIFO_PKTSTART;
			pkt_start_set = 1;
		}
		idx++;

		const uint8_t pr[4] = { fr->sync >> 24, (fr->sync >> 16) & 0xFF,
		                        (fr->sync >> 8) & 0xFF, fr->sync & 0xFF
		                      };
		memcpy(conf->tx_buffer + idx, pr, prlen);
		idx += prlen;

		/* Apply the stop bit */
		if (residual) {
			if (fr->pkt.msb_first) {
				conf->tx_buffer[idx - 1] = BIT(7 - residual) |
				                           (~((BIT(8 - residual) - 1)) & pr[prlen - 1]);
			} else {
				conf->tx_buffer[idx - 1] = BIT(residual)
				                           | ((BIT(residual) - 1) & pr[prlen - 1]);
			}
		}
	} else if (fr->sync_len % 8) {
		const uint8_t residual = fr->sync_len % 8;
		conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
		conf->tx_buffer[idx++] = 2;
		if (!pkt_start_set) {
			conf->tx_buffer[idx] =  AX5043_FIFO_PKTSTART;
			pkt_start_set = 1;
		}
		if (fr->sync_unencoded) {
			conf->tx_buffer[idx++] |= AX5043_FIFO_NOCRC | AX5043_FIFO_RESIDUE
			                          | AX5043_FIFO_RAW | AX5043_FIFO_UNENC;
		} else {
			conf->tx_buffer[idx++] |= AX5043_FIFO_NOCRC | AX5043_FIFO_RESIDUE
			                          | AX5043_FIFO_RAW;
		}
		/* Apply the stop bit */
		if (fr->pkt.msb_first) {
			conf->tx_buffer[idx++] = BIT(7 - residual) |
			                         (~((BIT(8 - residual) - 1)) & ((fr->sync >> 24) & 0xFF));
		} else {
			conf->tx_buffer[idx++] = BIT(residual)
			                         | ((BIT(residual) - 1) & ((fr->sync >> 24) & 0xFF));
		}
	}

	conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
	conf->tx_buffer[idx++] = cmd_len + 1;

	if (!pkt_start_set) {
		conf->tx_buffer[idx] =  AX5043_FIFO_PKTSTART;
		pkt_start_set = 1;
	}
	if (cmd_len == len) {
		conf->tx_buffer[idx] |= AX5043_FIFO_PKTEND;
	}
	idx++;

	memcpy(conf->tx_buffer + idx, in, cmd_len);
	idx += cmd_len;
	remaining -= cmd_len;

	while (remaining) {
		cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, remaining);
		remaining -= cmd_len;
		conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
		conf->tx_buffer[idx++] = cmd_len + 1;
		if (remaining == 0) {
			conf->tx_buffer[idx++] = AX5043_FIFO_PKTEND;
		} else {
			conf->tx_buffer[idx++] = 0;
		}
		memcpy(conf->tx_buffer + idx, in + len - remaining - cmd_len, cmd_len);
		idx += cmd_len;
	}
	return idx;
}

/**
 * Prepares the FIFO contents for the transmission of a HDLC frame. After
 * the call of this function \ref in buffer can be re-used, as this function
 * copies all the data into an internal buffer
 *
 * @param conf the AX5043 configuration handler
 * @param in the buffer with the <b>user</b> data
 * @param len the number of bytes of the <b>user</b> data
 * @return the number of bytes of the FIFO contents (data and FIFO commands
 * combined)
 */
static uint32_t
prepare_tx_hdlc(const struct ax5043_conf *conf, const uint8_t *in, uint32_t len)
{
	uint32_t idx = 0;
	uint32_t remaining = len;

	/*
	 * FIFO command for the HDLC preamble. The third byte corresponds the
	 * length of the preamble and is set by the TX routine for every frame
	 */
	conf->tx_buffer[0] = AX5043_FIFO_REPEATDATA_CMD;
	conf->tx_buffer[1] = AX5043_FIFO_PKTSTART | AX5043_FIFO_RAW | AX5043_FIFO_NOCRC;
	conf->tx_buffer[2] = conf->priv.tx.hdlc.preamble_len;
	conf->tx_buffer[3] = HDLC_SYNC_FLAG;
	idx = 4;

	uint8_t cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, len);
	/* Write the first FIFO cmd */
	conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
	conf->tx_buffer[idx++] = cmd_len + 1;
	if (cmd_len == len) {
		conf->tx_buffer[idx++] = AX5043_FIFO_PKTEND;
	} else {
		conf->tx_buffer[idx++] = 0;
	}
	memcpy(conf->tx_buffer + idx, in, cmd_len);
	idx += cmd_len;
	remaining -= cmd_len;

	while (remaining) {
		cmd_len = min(AX5043_FIFO_MAX_SIZE - 1, remaining);
		remaining -= cmd_len;
		conf->tx_buffer[idx++] = AX5043_FIFO_VARIABLE_DATA_CMD;
		conf->tx_buffer[idx++] = cmd_len + 1;
		if (remaining == 0) {
			conf->tx_buffer[idx++] = AX5043_FIFO_PKTEND;
		} else {
			conf->tx_buffer[idx++] = 0;
		}
		memcpy(conf->tx_buffer + idx,
		       in + len - remaining - cmd_len, cmd_len);
		idx += cmd_len;
	}

	/*
	 * FIFO command for the HDLC postamble. The third byte corresponds the
	 * length of the postable and is set by the TX routine for every frame
	 */
	conf->tx_buffer[idx++] = AX5043_FIFO_REPEATDATA_CMD;
	conf->tx_buffer[idx++] = AX5043_FIFO_PKTSTART | AX5043_FIFO_PKTEND
	                         | AX5043_FIFO_RAW | AX5043_FIFO_NOCRC;
	conf->tx_buffer[idx++] = conf->priv.tx.hdlc.postamble_len;
	conf->tx_buffer[idx++] = HDLC_SYNC_FLAG;
	return idx;
}

int
ax5043_tx_frame(struct ax5043_conf *conf, const uint8_t *in, uint32_t len,
                uint32_t timeout_ms)
{
	int ret = AX5043_OK;

	bool valid;
	/* Check if the user has configured properly the TX */
	ret = ax5043_tx_conf_valid(conf, &valid);
	if (ret) {
		return ret;
	}
	if (!valid) {
		return -AX5043_TX_INVALID;
	}

	/* Wait for the previous frame to be transmitted */
	while (g_tx_active) {
		ret++;
	}

	/* Based on the framing, prepare the FIFO data properly */
	uint32_t fifo_len = 0;
	switch (conf->priv.tx.framing) {
		case RAW:
			fifo_len = prepare_tx_frame_raw(conf, in, len);
			break;
		case RAW_SOFT:
			return -AX5043_NOT_IMPLEMENTED;
		case HDLC:
			fifo_len = prepare_tx_hdlc(conf, in, len);
			break;
		case RAW_PATTERN_MATCH:
			fifo_len = prepare_tx_frame(conf, in, len);
			break;
		case M_BUS:
		case MBUS_4_6:
		default:
			return -AX5043_INVALID_PARAM;
	}

	ret = tx_frame(conf, fifo_len, timeout_ms);
	return ret;
}

/**
 * Wait the crystal to become ready
 * @param conf the AX5043 configuration handler
 * @param timeout_ms the timeout in milliseconds
 * @return 0 on success or appropriate negative error code
 */
int
wait_xtal(struct ax5043_conf *conf, uint32_t timeout_ms)
{
	int ret;
	uint8_t val = 0x0;

	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}

	uint32_t start = ax5043_millis();
	while (!val) {
		ret = spi_read_8(conf,  &val, AX5043_REG_XTALSTATUS);
		if (ret) {
			return ret;
		}
		if ((ax5043_millis() - start) > timeout_ms) {
			return -AX5043_TIMEOUT;
		}
	}
	return AX5043_OK;
}

int
spi_read(struct ax5043_conf *conf, uint8_t *out, uint16_t reg, uint32_t len)
{
	int ret = AX5043_OK;
	reg &= 0xFFF;
	if (!ax5043_conf_valid(conf)) {
		return -AX5043_INVALID_PARAM;
	}
	const uint8_t mask = BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t hdr[2] =  {mask | (~mask & (reg >> 8)), reg & 0xFF};

	ax5043_spi_cs(0);
	ret = ax5043_spi_tx(hdr, 2);
	if (ret) {
		ax5043_spi_cs(1);
		return ret;
	}
	ret = ax5043_spi_rx(out, len);
	ax5043_spi_cs(1);
	return ret;
}

int
spi_read_8(struct ax5043_conf *conf, uint8_t *out, uint16_t reg)
{
	int ret;
	reg &= 0xFFF;
	ax5043_spi_cs(0);
	if (reg > 0xFF) {
		/* Long register access based on AND9347 p.5 */
		const uint8_t mask = BIT(6) | BIT(5) | BIT(4);
		uint8_t req[3] = {mask | (~mask & (reg >> 8)), reg & 0xFF, 0x0};
		uint8_t res[3] = {0x0, 0x0, 0x0};
		ret = ax5043_spi_trx(res, req, 3);
		*out = res[2];
	} else {
		/* Short register access based on AND9347 p.5 */
		uint8_t req[2] = {0x7F & reg, 0x0};
		uint8_t res[2] = {0x0, 0x0};
		ret = ax5043_spi_trx(res, req, 2);
		*out = res[1];
	}
	ax5043_spi_cs(1);
	return ret;
}

int
spi_read_16(struct ax5043_conf *conf, uint16_t *out, uint16_t reg)
{
	int ret;
	reg &= 0xFFF;
	const uint8_t mask = BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[4] = {mask | (~mask & (reg >> 8)), reg & 0xFF, 0x0, 0x0};
	uint8_t res[4];
	ax5043_spi_cs(0);
	ret = ax5043_spi_trx(res, req, 4);
	ax5043_spi_cs(1);
	uint16_t tmp = res[2];
	tmp = (tmp << 8) | res[3];
	*out = tmp;
	return ret;
}

int
spi_read_24(struct ax5043_conf *conf, uint32_t *out, uint16_t reg)
{
	int ret;
	reg &= 0xFFF;
	const uint8_t mask = BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[5] = {mask | (~mask & (reg >> 8)), reg & 0xFF, 0x0, 0x0, 0x0};
	uint8_t res[5];
	ax5043_spi_cs(0);
	ret = ax5043_spi_trx(res, req, 5);
	ax5043_spi_cs(1);
	uint32_t tmp = res[2];
	tmp = (tmp << 8) | res[3];
	tmp = (tmp << 8) | res[4];
	*out = tmp;
	return ret;
}

int
spi_read_32(struct ax5043_conf *conf, uint32_t *out, uint16_t reg)
{
	int ret;
	reg &= 0xFFF;
	const uint8_t mask = BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[6] = {mask | (~mask & (reg >> 8)), reg & 0xFF, 0x0, 0x0, 0x0, 0x0};
	uint8_t res[6];
	ax5043_spi_cs(0);
	ret = ax5043_spi_trx(res, req, 6);
	ax5043_spi_cs(1);
	uint32_t tmp = res[2];
	tmp = (tmp << 8) | res[3];
	tmp = (tmp << 8) | res[4];
	tmp = (tmp << 8) | res[5];
	*out = tmp;
	return ret;
}

int
spi_write(struct ax5043_conf *conf, uint16_t reg, const uint8_t *in,
          uint32_t len)
{
	int ret = AX5043_OK;
	reg &= 0xFFF;
	if (!ax5043_conf_valid(conf)) {
		return -AX5043_INVALID_PARAM;
	}
	const uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t hdr[2] =  { mask | (~mask & (reg >> 8)), reg & 0xFF};

	ax5043_spi_cs(0);
	ret = ax5043_spi_tx(hdr, 2);
	if (ret) {
		ax5043_spi_cs(1);
		return ret;
	}
	ret = ax5043_spi_tx(in, len);
	ax5043_spi_cs(1);
	return ret;
}

int
spi_write_8(struct ax5043_conf *conf, uint16_t reg, uint8_t in)
{
	int ret;
	reg &= 0xFFF;
	ax5043_spi_cs(0);
	if (reg > 0xFF) {
		const uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);
		/* Long register access based on AND9347 p.5 */
		uint8_t req[3] = {mask | (~mask & (reg >> 8)), reg & 0xFF, in};
		ret = ax5043_spi_tx(req, 3);
	} else {
		/* Short register access based on AND9347 p.5 */
		uint8_t req[2] = {BIT(7) | (0x7F & reg), in};
		ret = ax5043_spi_tx(req, 2);
	}
	ax5043_spi_cs(1);
	return ret;
}

int
spi_write_16(struct ax5043_conf *conf, uint16_t reg, uint16_t in)
{
	reg &= 0xFFF;
	const uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[4] = {mask | (~mask & (reg >> 8)), reg & 0xFF, (in >> 8), in};
	ax5043_spi_cs(0);
	int ret = ax5043_spi_tx(req, 4);
	ax5043_spi_cs(1);
	return ret;
}

int
spi_write_24(struct ax5043_conf *conf, uint16_t reg, uint32_t in)
{
	reg &= 0xFFF;
	const uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[5] = {mask | (~mask & (reg >> 8)), reg & 0xFF, (in >> 16),
	                  (in >> 8), in
	                 };
	ax5043_spi_cs(0);
	int ret = ax5043_spi_tx(req, 5);
	ax5043_spi_cs(1);
	return ret;
}

int
spi_write_32(struct ax5043_conf *conf, uint16_t reg, uint32_t in)
{
	reg &= 0xFFF;
	const uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);
	/* Prepare the long access header AND9347 p.5 */
	uint8_t req[6] = {mask | (~mask & (reg >> 8)), reg & 0xFF, (in >> 24),
	                  (in >> 16), (in >> 8), in
	                 };
	ax5043_spi_cs(0);
	int ret = ax5043_spi_tx(req, 6);
	ax5043_spi_cs(1);
	return ret;
}

/**
 * Sets properly some undocumented TX registers
 * @param conf  the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
static inline int
set_tx_black_magic_regs(struct ax5043_conf *conf)
{
	int ret;
	ret = spi_write_8(conf, 0xF00, 0x0F);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, 0xF0C, 0x0);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, 0xF1C, 0x07);
	if (ret) {
		return ret;
	}

	ret = spi_write_8(conf, 0xF44, 0x24);
	if (ret) {
		return ret;
	}

	/* Dafuq? Got it from RadioLab */
	ret = spi_write_8(conf, 0xF18, 0x06);
	return ret;
}

/**
 * Controls the ANTSEL pin
 * @param conf the AX5043 configuration handler
 * @param weak_pullup set to 1 to enable the weak pullup.
 * @param invert set to 1 to revert the ANTSEL logic
 * @param pfantsel use the pfantsel_t enumeration to set properly the behavior
 * of the ANTSEL pin
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_pinfuncantsel(struct ax5043_conf *conf, uint8_t weak_pullup,
                         uint8_t invert, pfantsel_t pfantsel)
{
	uint8_t b = 0x0;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	switch (pfantsel) {
		case ANTSEL_OUTPUT_0:
		case ANTSEL_OUTPUT_1:
		case ANTSEL_OUTPUT_BB_TUBE_CLK:
		case ANTSEL_OUTPUT_DAC:
		case ANTSEL_OUTPUT_DIVERSITY:
		case ANTSEL_OUTPUT_EXT_TCXO_EN:
		case ANTSEL_OUTPUT_TEST_OBS:
		case ANTSEL_OUTPUT_Z:
			b = pfantsel;
			b |= (invert & 0x1) << 6;
			b |= (weak_pullup & 0x1) << 7;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	return spi_write_8(conf, AX5043_REG_PINFUNCANTSEL, b);
}

/**
 * Controls the PWRAMP pin
 * @param conf the AX5043 configuration handler
 * @param weak_pullup set to 1 to enable the weak pullup.
 * @param invert set to 1 to revert the PWRAMP logic
 * @param pfpwramp use the pfpwwramp_t enumeration to set properly the behavior
 * of the PWRAMP pin
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_pinfuncpwramp(struct ax5043_conf *conf, uint8_t weak_pullup,
                         uint8_t invert, pfpwwramp_t pfpwramp)
{
	uint8_t b = 0x0;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	switch (pfpwramp) {
		case PWRAMP_OUTPUT_0:
		case PWRAMP_OUTPUT_1:
		case PWRAMP_OUTPUT_Z:
		case PWRAMP_INPUT_DIBIT_SYNC:
		case PWRAMP_OUTPUT_DIBIT_SYNC:
		case PWRAMP_OUTPUT_DAC:
		case PWRAMP_OUTPUT_PWRAMP:
		case PWRAMP_OUTPUT_TCXO_EN:
		case PWRAMP_OUTPUT_TEST_OBS:
			b = pfpwramp;
			b |= (invert & 0x1) << 6;
			b |= (weak_pullup & 0x1) << 7;
			break;
		default:
			return -AX5043_INVALID_PARAM;
	}
	return spi_write_8(conf, AX5043_REG_PINFUNCPWRAMP, b);
}

/**
 * Sets the value of the power amplifier pin.
 * @note This may not be the actual output of the PWRAMP pin. This depends also
 * on the configuration applied by the \ref ax5043_set_pinfuncpwramp() function
 *
 * @param conf the AX5043 configuration handler
 * @param val should be 0 or 1. The actual state of the PWRAMP pin depends on
 * the configuration applied by ax5043_set_pinfuncpwramp()
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_pwramp(struct ax5043_conf *conf, uint8_t val)
{
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	return spi_write_8(conf, AX5043_REG_PWRAMP, val & 0x1);
}

/**
 * Set the packet accept flags
 * @param conf the AX5043 configuration handler
 * @param flags the flags as described in the PKTACCEPTFLAGS register
 * @return 0 on success or negative error code
 */
int
ax5043_set_pktacceptflags(struct ax5043_conf *conf, uint8_t flags)
{
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	return spi_write_8(conf, AX5043_REG_PKTACCEPTFLAGS, flags);
}

/**
 * Configures the IC for CW transmission
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_conf_cw(struct ax5043_conf *conf)
{
	struct tx_params p;
	/* Set modulation to ASK to emulate CW */
	p.mod = ASK_COHERENT;
	p.shaping = UNSHAPED;
	p.rf_out_mode = TXSE;
	p.pout_dBm = -10;
	p.framing = RAW_SOFT;
	memset(&p.raw.enc, 0, sizeof(struct encoding));
	p.raw.crc.mode = CRC_OFF;
	/* FIXME! */
	p.baudrate = 19200;
	return ax5043_conf_tx(conf, &p);
}

int
ax5043_tx_cw(struct ax5043_conf *conf, uint32_t duration_ms)
{
	int ret = AX5043_OK;

	uint8_t data_cmd[4] =  { AX5043_FIFO_REPEATDATA_CMD,
	                         AX5043_FIFO_PKTSTART | AX5043_FIFO_RAW | AX5043_FIFO_NOCRC, 1, 0xFF
	                       };
	uint8_t val;
	uint32_t start = ax5043_millis();

	/* Wait for the previous frame to be transmitted */
	while (g_tx_active) {
		ret++;
	}

	/* Set AX5043 to FULLTX mode */
	ret = ax5043_set_power_mode(conf, FULLTX);
	if (ret) {
		return ret;
	}

	wait_xtal(conf, 100);

	/* Wait for the FIFO to become ready */
	val = 0;
	while (!val) {
		spi_read_8(conf, &val, AX5043_REG_POWSTAT);
		/* Select only the modem power state */
		val &= AX5043_SVMODEM;
		if (ax5043_millis() - start > duration_ms) {
			ax5043_set_power_mode(conf, POWERDOWN);
			ret = -AX5043_TIMEOUT;
			break;
		}
	}

	g_tx_active = 1;
	while (ax5043_millis() - start < duration_ms) {
		uint16_t ffree = 0;
		while (ffree < 8) {
			spi_read_16(conf, &ffree, AX5043_REG_FIFOFREE1);
		}
		ret = spi_write(conf, AX5043_REG_FIFODATA, data_cmd, 4);
		ret = spi_write_8(conf, AX5043_REG_FIFOSTAT, AX5043_FIFO_COMMIT_CMD);
	}
	tx_frame_end(conf);
	return AX5043_OK;
}

/**
 * Returns the RSSI and the background RSSI
 * @param conf the AX5043 configuration handler
 * @param rssi pointer to store the RSSI
 * @param bgnrssi pointer to store the background RSSI
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_rssi(struct ax5043_conf *conf, float *rssi, float *bgnrssi)
{
	int ret = AX5043_OK;
	if (!ax5043_ready(conf)) {
		return -AX5043_NOT_READY;
	}
	uint16_t val = 0;
	ret = spi_read_16(conf, &val, AX5043_REG_RSSI);
	if (ret) {
		return ret;
	}
	*rssi = val >> 8;
	*bgnrssi = val & 0xFF;
	return AX5043_OK;
}

static int
tx_irq()
{
	int ret;
	size_t avail;
	uint32_t tmp;
	uint16_t reg0 = 0x0;
	uint16_t reg1 = 0x0;

	if (!ax5043_conf) {
		return AX5043_OK;
	}

	/* Read both IRQREQUEST1 & RADIOEVENTREQ1 at once, to save some cycles */
	ret = spi_read_32(ax5043_conf, &tmp, AX5043_REG_IRQREQUEST1);
	if (ret) {
		return ret;
	}
	reg0 = tmp >> 16;
	reg1 = tmp & 0xFFFF;


	/* TX is done! */
	if (reg1 & AX5043_REVRDONE) {
		tx_frame_end(ax5043_conf);
		return AX5043_OK;
	}

	/* If FIFO has free space fill in data */
	if (reg0 & AX5043_IRQRFIFOTHRFREE) {
		avail = min(AX5043_FIFO_FREE_THR, g_tx_remaining);
		if (avail == g_tx_remaining) {
			/* Mask the FIFO free IRQ as it is not needed anymore */
			tx_fifo_irq_enable(ax5043_conf, 0);
		}
		spi_write(ax5043_conf, AX5043_REG_FIFODATA,
		          ax5043_conf->tx_buffer + g_tx_buf_idx,
		          avail);
		/* Commit to FIFO ! */
		ret = spi_write_8(ax5043_conf, AX5043_REG_FIFOSTAT,
		                  AX5043_FIFO_COMMIT_CMD);

		g_tx_remaining -= avail;
		g_tx_buf_idx += avail;
	}
	return AX5043_OK;
}

static void
get_rx_info(struct ax5043_conf *conf, uint8_t *b)
{
	uint32_t idx = 0;
	while (idx < 13) {
		uint8_t cmd = b[idx++];
		switch (cmd) {
			/* RSSI */
			case 0x31:
				conf->rx_status.rssi = b[idx++];
				break;
			/* FREQOFFS */
			case 0x52:
				conf->rx_status.freq_tracking = b[idx++];
				conf->rx_status.freq_tracking <<= 8;
				conf->rx_status.freq_tracking = b[idx++];
				break;
			/* DATARATE */
			case 0x74:
				conf->rx_status.datarate_tracking = b[idx++];
				conf->rx_status.datarate_tracking <<= 8;
				conf->rx_status.datarate_tracking = b[idx++];
				conf->rx_status.datarate_tracking <<= 8;
				conf->rx_status.datarate_tracking = b[idx++];
				break;
			/* RFFREQOFFS */
			case 0x73:
				conf->rx_status.rf_freq_tracking = b[idx++];
				conf->rx_status.rf_freq_tracking <<= 8;
				conf->rx_status.rf_freq_tracking = b[idx++];
				conf->rx_status.rf_freq_tracking <<= 8;
				conf->rx_status.rf_freq_tracking = b[idx++];
				break;
		}
	}
}

static int
handle_rx_fifo()
{
	int ret;
	uint8_t cmd = 0;
	uint16_t fill = 0;
	ret = spi_read_16(ax5043_conf, &fill, AX5043_REG_FIFOCOUNT1);
	if (ret) {
		return ret;
	}

	while (fill) {
		ret = spi_read_8(ax5043_conf, &cmd, AX5043_REG_FIFODATA);
		if (ret) {
			return ret;
		}
		fill--;
		/* Search for a valid command */
		if (cmd == AX5043_FIFO_VARIABLE_DATA_CMD) {
			if (fill < 2) {
				return -AX5043_FIFO_ERROR;
			}
			uint8_t tmp[2] = {0, 0};
			ret = spi_read(ax5043_conf, tmp, AX5043_REG_FIFODATA, 2);
			if (ret) {
				return ret;
			}
			fill -= 2;
			/*
			 * Make sure that the data of the frame are indeed in
			 * the FIFO. Note that the length field includes the
			 * flags byte
			 */
			if (fill < tmp[0] - 1) {
				return -AX5043_FIFO_ERROR;
			}

			/*
			 * Check the PKTSTART. If it set, reset any previous outstanding
			 * frame and start again
			 */
			if (tmp[1] & AX5043_FIFO_PKTSTART) {
				ax5043_conf->priv.rx_buffer_idx = 0;
			} else {
				/*
				 * If the RX process did not identified previously a frame
				 * start, read all the remaining data field from the FIFO
				 * and abort
				 */
				if (ax5043_conf->priv.rx_buffer_idx == 0) {
					ret = spi_read(ax5043_conf,
					               ax5043_conf->rx_buffer + ax5043_conf->priv.rx_buffer_idx,
					               AX5043_REG_FIFODATA,
					               tmp[0] - 1);
					return ret;
				}

			}
			ret = spi_read(ax5043_conf,
			               ax5043_conf->rx_buffer + ax5043_conf->priv.rx_buffer_idx,
			               AX5043_REG_FIFODATA, tmp[0] - 1);
			if (ret) {
				return ret;
			}
			ax5043_conf->priv.rx_buffer_idx += tmp[0] - 1;
			fill -= tmp[0] - 1;

			/*
			 * If this was the end of the frame, make it available to the
			 * upper layers
			 */
			if (tmp[1] & AX5043_FIFO_PKTEND) {
				ax5043_rx_complete_callback(ax5043_conf->rx_buffer,
				                            ax5043_conf->priv.rx_buffer_idx);
				ax5043_conf->priv.rx_buffer_idx = 0;

				/* If the packet metadata are in the FIFO
				 * update the internal statistics
				 */
				if (fill == 13) {
					spi_read(ax5043_conf, ax5043_conf->rx_buffer, AX5043_REG_FIFODATA, 13);
					get_rx_info(ax5043_conf, ax5043_conf->rx_buffer);
				}
			}
		}
		/* Perhaps more data are available */
		ret = spi_read_16(ax5043_conf, &fill, AX5043_REG_FIFOCOUNT1);
		if (ret) {
			return ret;
		}

	}
	return AX5043_OK;
}

static int
rx_irq()
{
	int ret;
	uint32_t tmp = 0;

	/* Read both IRQREQUEST1 & RADIOEVENTREQ1 at once, to save some cycles */
	ret = spi_read_32(ax5043_conf, &tmp, AX5043_REG_IRQREQUEST1);
	if (ret) {
		return ret;
	}

	ret = handle_rx_fifo();
	if (ret) {
		return ret;
	}
	return AX5043_OK;
}

/**
 * The IRQ handler for the AX5043
 * @return 0 on success, or appropriate negative error code
 */
int
ax5043_irq_callback()
{
	if (!ax5043_conf) {
		return AX5043_OK;
	}
	if (ax5043_conf->priv.pwr_mode == FULLTX) {
		return tx_irq();
	}
	if (ax5043_conf->priv.pwr_mode == FULLRX) {
		return rx_irq();
	}
	return AX5043_OK;
}

/******************************************************************************
 **************************** Used only for debugging  ************************
 ******************************************************************************/
void
init_dump_regs(struct reg_dump *dump)
{
#if AX5043_DEBUG
	uint16_t cnt = 0;
	/* Revision & Interface Probing */
	dump->regs[cnt++].reg = 0x0;
	dump->regs[cnt++].reg = 0x1;
	/* Operating Mode */
	dump->regs[cnt++].reg = 0x2;
	/* Voltage Regulator */
	for (uint16_t j = 0x3; j <= 0x5; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Interrupt Control */
	for (uint16_t j = 0x6; j <= 0xF; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Modulation & Framing */
	for (uint16_t j = 0x10; j <= 0x12; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x14; j <= 0x17; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Forward Error Correction */
	for (uint16_t j = 0x18; j <= 0x1A; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Status */
	for (uint16_t j = 0x1C; j <= 0x1D; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Pin Configuration */
	for (uint16_t j = 0x20; j <= 0x27; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* FIFO */
	for (uint16_t j = 0x28; j <= 0x2F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Synthesizer */
	for (uint16_t j = 0x30; j <= 0x39; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x3B; j <= 0x3F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Signal Strength */
	for (uint16_t j = 0x40; j <= 0x43; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Tracking */
	for (uint16_t j = 0x45; j <= 0x4B; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x4D; j <= 0x53; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Timer */
	for (uint16_t j = 0x59; j <= 0x5B; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Wakeup Timer */
	for (uint16_t j = 0x68; j <= 0x6E; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Parameters */
	for (uint16_t j = 0x100; j <= 0x118; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Parameter Set 0 */
	for (uint16_t j = 0x120; j <= 0x12F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Parameter Set 1 */
	for (uint16_t j = 0x130; j <= 0x13F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Parameter Set 2 */
	for (uint16_t j = 0x140; j <= 0x14F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Receiver Parameter Set 3 */
	for (uint16_t j = 0x150; j <= 0x15F; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Transmitter Parameters */
	for (uint16_t j = 0x160; j <= 0x171; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* PLL Parameters */
	for (uint16_t j = 0x181; j <= 0x183; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Crystal oscillator */
	dump->regs[cnt++].reg = 0x184;
	/* Baseband */
	dump->regs[cnt++].reg = 0x188;
	dump->regs[cnt++].reg = 0x189;
	/* Packet format */
	for (uint16_t j = 0x200; j <= 0x20B; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Pattern match */
	for (uint16_t j = 0x210; j <= 0x216; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x218; j <= 0x219; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x21C; j <= 0x21E; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Packet Controller */
	for (uint16_t j = 0x220; j <= 0x221; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x223; j <= 0x233; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* General Purpose ADC */
	for (uint16_t j = 0x300; j <= 0x301; j++) {
		dump->regs[cnt++].reg = j;
	}
	for (uint16_t j = 0x308; j <= 0x309; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* Low Power Oscillator Calibration */
	for (uint16_t j = 0x310; j <= 0x319; j++) {
		dump->regs[cnt++].reg = j;
	}
	/* DAC */
	for (uint16_t j = 0x330; j <= 0x332; j++) {
		dump->regs[cnt++].reg = j;
	}
#endif
}

/**
 * Creates a dump of all the AX5043 registers inside the dump structure of the
 * @p conf
 * @param conf the AX5043 configuration handler
 */
void
ax5043_dump_regs(struct ax5043_conf *conf)
{
#if AX5043_DEBUG
	if (!conf) {
		return;
	}

	for (uint16_t i = 0; i < AX5043_DUMP_REGS_NUM; i++) {
		spi_read_8(conf, &conf->dump.regs[i].val, conf->dump.regs[i].reg);
	}
#endif
}

static char
hex_digit(uint8_t c)
{
	if (c > 9) {
		return (c - 10) + 65;
	}
	return c + 48;
}

/**
 * Constructs a string with the dump of all available registers
 * @param s a string with enough space to hold the dump
 * @param conf the AX5043 device handle
 * @param slen the maximum length of the string
 * @return the string with the dump or NULL in case of error. In case the
 * available space in not enough, the output is truncated
 */
char *
ax5043_dump_regs_str(char *s, const struct ax5043_conf *conf, int slen)
{
#if AX5043_DEBUG
	if (!s || slen < 4) {
		return 0;
	}
	int idx = 0;
	s[idx++] = '{';
	s[idx++] = '\n';

	for (uint16_t i = 0; i < AX5043_DUMP_REGS_NUM; i++) {
		if (idx +  11 > slen - 2) {
			s[idx] = 0;
			return s;
		}
		s[idx++] = '0';
		s[idx++] = 'x';
		s[idx++] = hex_digit((conf->dump.regs[i].reg >> 8 & 0xF));
		s[idx++] = hex_digit((conf->dump.regs[i].reg >> 4 & 0xF));
		s[idx++] = hex_digit((conf->dump.regs[i].reg  & 0xF));
		s[idx++] = ':';
		s[idx++] = '0';
		s[idx++] = 'x';
		s[idx++] = hex_digit((conf->dump.regs[i].val >> 4 & 0xF));
		s[idx++] = hex_digit((conf->dump.regs[i].val  & 0xF));
		s[idx++] = '\n';
	}
	s[idx++] = '}';
	s[idx++] = '\n';
	s[idx++] = 0;
	return s;
#endif
}

